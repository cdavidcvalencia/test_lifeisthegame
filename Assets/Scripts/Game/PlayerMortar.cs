using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMortar : MonoBehaviour {

    public Transform muzzle;
    public float cooldown;
    public float power;

    bool readyToFire = true;
    float cooldownTimer = 0;

    Pool pool;

    private void Start() {
        pool = PoolManager.instance.GetPool("MortarBullet");
    }

    private void Update() {
        if (!readyToFire) {
            cooldownTimer += Time.deltaTime;
            if (cooldownTimer >= cooldown) {
                readyToFire = true;
                cooldownTimer = 0;
            }
        }
        if (Input.GetButton("Fire1") && readyToFire) {
            MortarBullet bullet = pool.Get().GetComponent<MortarBullet>();
            bullet.Fire(muzzle, power);
            readyToFire = false;
        }
    }

}
