using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLook : MonoBehaviour
{

    [Header("References")]
    public Transform player;

    [Header("Config")]
    public float sensitivity = 100;

    float xRotation;

    void Update() {
        float deltaTime = Time.deltaTime;
        float mouseX = Input.GetAxis("Mouse X") * sensitivity * deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * sensitivity * deltaTime;
        player.Rotate(Vector3.up, mouseX);
        xRotation -= mouseY;
        xRotation = Mathf.Clamp(xRotation, -90, 90);
        transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
    }

}
