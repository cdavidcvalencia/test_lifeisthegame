using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAntiGravity : MonoBehaviour {

    public Transform muzzle;
    public float cooldown;
    public float bulletSpeed;
    public float power;
    public float duration;

    bool readyToFire = true;
    float cooldownTimer = 0;

    Pool pool;

    private void Start() {
        pool = PoolManager.instance.GetPool("AntiGravityBullet");
    }

    private void Update() {
        if (!readyToFire) {
            cooldownTimer += Time.deltaTime;
            if (cooldownTimer >= cooldown) {
                readyToFire = true;
                cooldownTimer = 0;
            }
        }
        if (Input.GetButton("Fire1") && readyToFire) {
            AntiGravityBullet bullet = pool.Get().GetComponent<AntiGravityBullet>();
            bullet.Fire(muzzle, bulletSpeed, power, duration);
            readyToFire = false;
        }
    }

}
