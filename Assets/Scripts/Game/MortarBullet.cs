using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MortarBullet : MonoBehaviour {

    public LayerMask targetMask;

    PoolItem poolItem;
    Rigidbody body;

    private void Awake() {
        poolItem = GetComponent<PoolItem>();
        body = GetComponent<Rigidbody>();
        Deactivate();
    }

    private void OnTriggerEnter(Collider other) {
        if (((1 << other.gameObject.layer) & targetMask) != 0) {
            other.GetComponent<Target>().MortarHit(body.velocity);
        }
        Deactivate();
    }

    public void Deactivate() {
        gameObject.SetActive(false);
        poolItem.isActive = false;
    }

    public void Fire(Transform muzzle, float power) {
        gameObject.SetActive(true);
        body.isKinematic = true;
        body.velocity = Vector3.zero;
        transform.parent = muzzle;
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;
        transform.parent = null;
        body.isKinematic = false;
        body.AddForce(transform.forward * power);
    }

}
