using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AntiGravityBullet : MonoBehaviour {

    public LayerMask targetMask;
    public float lifeTime;

    PoolItem poolItem;
    Rigidbody body;
    float duration;
    float power;
    float timer;

    private void Awake() {
        poolItem = GetComponent<PoolItem>();
        body = GetComponent<Rigidbody>();
        Deactivate();
    }

    private void Update() {
        timer += Time.deltaTime;
        if (timer >= duration) {
            Deactivate();
        }
    }

    private void OnTriggerEnter(Collider other) {
        if (((1 << other.gameObject.layer) & targetMask) != 0) {
            other.GetComponent<Target>().AntiGravityHit(duration, power);
        }
        Deactivate();
    }

    public void Deactivate() {
        gameObject.SetActive(false);
        poolItem.isActive = false;
    }

    public void Fire(Transform muzzle, float speed, float inPower, float inDuration) {
        power = inPower;
        timer = 0;
        duration = inDuration;
        gameObject.SetActive(true);
        body.isKinematic = true;
        body.velocity = Vector3.zero;
        transform.parent = muzzle;
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;
        transform.parent = null;
        body.isKinematic = false;
        body.AddForce(transform.forward * speed);
    }

}
