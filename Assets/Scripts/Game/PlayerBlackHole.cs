using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBlackHole : MonoBehaviour
{
    public Transform muzzle;
    public float cooldown;
    public float bulletSpeed;
    public float power;
    public float duration;
    public float range;

    bool readyToFire = true;
    float cooldownTimer = 0;

    Pool pool;

    private void Start() {
        pool = PoolManager.instance.GetPool("BlackHoleBullet");
    }

    private void Update() {
        if (!readyToFire) {
            cooldownTimer += Time.deltaTime;
            if (cooldownTimer >= cooldown) {
                readyToFire = true;
                cooldownTimer = 0;
            }
        }
        if (Input.GetButton("Fire1") && readyToFire) {
            BlackHoleBullet bullet = pool.Get().GetComponent<BlackHoleBullet>();
            bullet.Fire(muzzle, bulletSpeed, power, duration, range);
            readyToFire = false;
        }
    }
}
