using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CharacterSelectManager : MonoBehaviour {

    public static CharacterSelectManager instance;

    [Header("Data")]
    public DanceScriptable danceData;

    [Header("Prefabs")]
    public DanceButton danceButtonPrefab;

    [Header("UI")]
    public Transform danceButtonContainer;

    public void Awake() {
        instance = this;
        int numDances = ArrayUtil.Length(danceData.dances);
        for (int idx = 0; idx < numDances; idx++) {
            Dance dance = danceData.dances[idx];
            DanceButton danceButton = Instantiate(danceButtonPrefab, danceButtonContainer);
            danceButton.labelLocale.StringReference = dance.localizedName;
            danceButton.danceNumber = dance.danceNumber;
        }
    }

    public void ChangeDance(int danceNumber) {
        CharacterAvatar.instance.animator.SetInteger("Dance", danceNumber);
    }

    public void Play() {
        SceneManager.LoadScene("MainGame");
    }

    public enum Status {
        CharacterCreation,
        Game
    }

}
