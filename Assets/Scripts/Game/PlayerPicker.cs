using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPicker : MonoBehaviour {

    public WeaponScriptable weaponData;

    public float pickDistance = 1f;
    public LayerMask pickableMask;

    public Collider[] pickableResults=new Collider[1];

    public PlayerMortar mortar;
    public PlayerAntiGravity antiGravity;
    public PlayerBlackHole blackHole;

    public Transform weaponHolder;
    Transform prevWeaponHolder;
    Quaternion prevWeaponRotation;
    Vector3 prevWeaponPosition;
    Vector3 prevWeaponScale;

    Pickable lastPickable;

    private void Start() {
        mortar.cooldown = weaponData.MortarCooldown;
        mortar.power = weaponData.MortarPower;

        antiGravity.cooldown = weaponData.antiGravityCooldown;
        antiGravity.bulletSpeed = weaponData.antiGravityBulletSpeed;
        antiGravity.power = weaponData.antiGravityPower;
        antiGravity.duration = weaponData.antiGravityDuration;

        blackHole.cooldown = weaponData.blackHoleCooldown;
        blackHole.bulletSpeed = weaponData.blackHoleBulletSpeed;
        blackHole.power = weaponData.blackHolePower;
        blackHole.duration = weaponData.blackHoleDuration;
        blackHole.range = weaponData.blackHoleRange;

    }

    private void Update() {
        int numResults = Physics.OverlapSphereNonAlloc(transform.position, pickDistance, pickableResults, pickableMask);
        if (numResults > 0) {
            Pickable pickable = pickableResults[0].GetComponent<Pickable>();
            if (pickable && !pickable.picked) {
                if (lastPickable) {
                    lastPickable.Model.parent = prevWeaponHolder;
                    lastPickable.Model.localPosition = prevWeaponPosition;
                    lastPickable.Model.localRotation = prevWeaponRotation;
                    lastPickable.Model.localScale = prevWeaponScale;
                    lastPickable.SetPicked(false);
                }
                prevWeaponHolder = pickable.Model.parent;
                prevWeaponRotation = pickable.Model.localRotation;
                prevWeaponPosition = pickable.Model.localPosition;
                prevWeaponScale = pickable.Model.localScale;
                pickable.Model.parent = weaponHolder;
                pickable.Model.localPosition = prevWeaponPosition;
                pickable.Model.localRotation = prevWeaponRotation;
                pickable.Model.localScale = prevWeaponScale;
                pickable.SetPicked(true);
                mortar.enabled = pickable.pickableType == PickableType.mortar;
                antiGravity.enabled = pickable.pickableType == PickableType.antiGravity;
                blackHole.enabled = pickable.pickableType == PickableType.blackHole;
                lastPickable = pickable;
            }
        }
    }

}
