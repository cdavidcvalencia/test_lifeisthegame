using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {

    [Header("References")]
    public Transform avatarContainer;

    public void Awake() {
        Cursor.lockState = CursorLockMode.Locked;
        CharacterAvatar.instance.transform.parent = avatarContainer;
        CharacterAvatar.instance.transform.localPosition = Vector3.zero;
    }

}
