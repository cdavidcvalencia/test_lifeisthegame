using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ArrayUtil
{
	public static int Length<T>(T[] array) {
		if (array == null) return 0;
		return array.Length;
	}
}
