using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Localization.Components;
using UnityEngine.UI;

public class DanceButton : MonoBehaviour {

    [Header("References")]
    public LocalizeStringEvent labelLocale;
    public Button button;

    public int danceNumber;

    public void Start() {
        button.onClick.AddListener(OnClick);
    }

    public void OnClick() {
        CharacterSelectManager.instance.ChangeDance(danceNumber);
    }

}
