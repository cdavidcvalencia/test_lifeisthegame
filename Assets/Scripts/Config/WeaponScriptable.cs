using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Weapons", menuName = "LIfeIsTheGame/Weapons")]
public class WeaponScriptable : ScriptableObject {

    public float blackHoleCooldown;
    public float blackHoleBulletSpeed;
    public float blackHolePower;
    public float blackHoleDuration;
    public float blackHoleRange;

    public float antiGravityCooldown;
    public float antiGravityBulletSpeed;
    public float antiGravityPower;
    public float antiGravityDuration;

    public float MortarCooldown;
    public float MortarPower;

}
